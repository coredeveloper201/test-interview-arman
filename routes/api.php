<?php

use Illuminate\Http\Request;
use Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->namespace('API')->group(function () {
  // Login
  Route::post('/login','AuthController@postLogin');
  // Register
  Route::post('/register','AuthController@postRegister');
  // Protected with APIToken Middleware
  Route::middleware('APIToken')->group(function () {
    // Logout
    Route::post('/logout','AuthController@postLogout');
  });
});



// date_check

Route::post('check_date', [
    'middleware' => 'AuthMiddleware', function(Request $request) {

    $date_check = $request['datetime'];
	$todaydate = date('Y-m-d h:m:i');


	if($todaydate > $date_check){

		$feedback = [
           'status'     => "error",
           'message'    => "Please provide a future date"
        ]; 

	}else{
        $diff = abs(strtotime($date_check) - strtotime($todaydate));

	$years = floor($diff / (365*60*60*24));
	$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
	$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
	$value = "Year :".$years." Month :".$months." Day :".$days;

		$feedback = [
           'status'     => "success",
           'message'    => "Valid data",
           'param'		=>  $value
        ]; 
	    
    }
    
    return $feedback;
}]);





// api validation

Route::post('facebook', [

    'middleware' => 'AuthMiddleware', function(Request $request) {

    $data = $request->input('data');
    $validator = $this->validator($data);
     

	if($validator->fails()) {

		$feedback = [
           'status'     => "error",
           'message'    => "Please provide valid data"
        ]; 

	}else{

		$feedback = [
           'status'     => "success",
           'message'    => "Valid data",
        ]; 
	    
    }
    
    return $feedback;
}]);


private function validator($data)
    {
        return Validator::make($data, [
          'title'           => 'required|max:500',
          'site_name'       => 'required|max:500',
          'site_logo'       => 'mimes:jpeg,jpg,png|required',
          'site_description'=> 'required|max:2000',
        ]);
    }